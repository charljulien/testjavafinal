package App;

import entities.animal_entities.*;
import entities.plant_entities.*;
import service.ForestNotebook;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class NatureApp {
    public static void main(String[] args){
        ForestNotebook notebook = new ForestNotebook();

        notebook.addPlant(new Tree("Baobabus Boaobabus",15, LeafType.NEEDLE));
        notebook.addPlant(new Tree("Eucalyptus Jurassicus Anus",15, LeafType.NEEDLE));
        notebook.addPlant(new Flower("Rosea Plantea Menoposea",5, Scent.ORANGE));
        notebook.addPlant(new Bush("Evictus Spartacus Cunnilingus",5, LeafType.SPEAR));
        notebook.addPlant(new Weed("Snoooop Dooogggg",12,10));

        notebook.addAnimals(new Herbivore("Pikachu Elektronikus",10,10,10));
        notebook.addAnimals(new Herbivore("SocialJusticeWarriorus Veganus",1,2,20));
        notebook.addAnimals(new Herbivore("Veganus Veganus",0,0,1));
        notebook.addAnimals(new Herbivore("Bartus de Weverus",10,10,10));       //.addPlantToDiet(new Bush("Evictus Spartacus Cunnilingus",5, LeafType.SPEAR) maar werkt niet
        notebook.addAnimals(new Carnivore("Maggus de Blockus",150,12,12,100));
        notebook.addAnimals(new Carnivore("Tyranus Diplodocus Cunnilingus",100,50,20,80));
        notebook.addAnimals(new Carnivore("Tyrus Tittus Klijnus Tyranus",50,25,10,40));
        notebook.addAnimals(new Carnivore("Tyrus Tyrus Papyrus",10,25,50,20));
        notebook.addAnimals(new Omnivore("Homo Erectus Phallus",10,10,10,20));

        notebook.getAnimalCount();
        notebook.getPlantCount();
        notebook.printNotebook();
        notebook.sortAnimalsByName();
        notebook.sortPlantByName();

        notebook.getCarnivores();
        notebook.getHerbivores();
        notebook.getOmnivores();
    }
}
