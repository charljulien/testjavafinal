package service;

import entities.animal_entities.*;
import entities.plant_entities.*;

import java.util.*;

public class ForestNotebook {
    private List<Animal> carnivores = new LinkedList<Animal>();
    private List<Animal> omnivores = new LinkedList<Animal>();
    private List<Animal> herbivores = new LinkedList<Animal>();
    public int plantCount;
    private int animalCount;
    private List<Animal> animals = new LinkedList<>();
    private List<Plant> plants = new LinkedList<>();

    public ForestNotebook() {
    }

    public List<Animal> getCarnivores() {
        System.out.println("List of Carnivores");
        carnivores.forEach(System.out::println);
        System.out.print("\n");
        return carnivores;
    }
    public void setCarnivores(List<Animal> carnivores) {
        this.carnivores = carnivores;
    }

    public List<Animal> getOmnivores() {
        System.out.println("List of Omnivores");
        omnivores.forEach(System.out::println);
        System.out.print("\n");
        return omnivores;
    }
    public void setOmnivores(List<Animal> omnivores) {
        this.omnivores = omnivores;
    }

    public List<Animal> getHerbivores() {
        System.out.println("List of Herbivores");
        herbivores.forEach(System.out::println);
        System.out.print("\n");
        return herbivores;
    }
    public void setHerbivores(List<Animal> herbivores) {
        this.herbivores = herbivores;
    }

    public int getPlantCount() {
        System.out.println(("plantCount = " + plantCount));
        System.out.print("\n");
        return plantCount;
    }
    public int getAnimalCount() {
        System.out.println(("animalCount = " + animalCount));
        System.out.print("\n");
        return animalCount;
    }

    public void addAnimals(Animal animal){
        if (animal!=null)
            animals.add(animal);

        if(animal instanceof Omnivore)
            omnivores.add(animal);
        else if (animal instanceof Herbivore)
            herbivores.add(animal);
        else
            carnivores.add(animal);
        animalCount++;
    }

    public void addPlant(Plant plant){
        if (plant!=null){
            plants.add(plant);
        }
        plantCount++;
    }

    public void printNotebook(){
        System.out.println("NOTEBOOK");
        animals.forEach(System.out::println);
        plants.forEach(System.out::println);
        System.out.print("\n");
    }

    public void sortAnimalsByName(){
        Comparator<Animal> comparator = Comparator.comparing(Animal::getName);
        SortedSet<Animal> animalSortedSet = new TreeSet<>(comparator);
        animalSortedSet.addAll(animals);
        System.out.println("LIST OF ANIMALS SORTED");
        animalSortedSet.forEach(System.out::println);
        System.out.print("\n");
    }

    public void sortPlantByName(){
        Comparator<Plant> comparator = Comparator.comparing(Plant::getName);
        SortedSet<Plant> plantSortedSet = new TreeSet<>(comparator);
        plantSortedSet.addAll(plants);
        System.out.println("LIST OF PLANTS SORTED");
        plantSortedSet.forEach(System.out::println);
        System.out.print("\n");
    }
}
