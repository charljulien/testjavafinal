package entities.animal_entities;

import entities.plant_entities.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Omnivore extends Animal {
    private Set<Plant> plantDiet = new HashSet<>();
    private double maxFoodSize;

    public Omnivore(String name, double maxFoodSize) {
        super(name);
        this.maxFoodSize = maxFoodSize;
    }
    public Omnivore(String name, double weight, double height, double length, double maxFoodSize) {
        super(name, weight, height, length);
        this.maxFoodSize = maxFoodSize;
    }

    public Set<Plant> getPlantDiet() {
        return plantDiet;
    }
    public void setPlantDiet(Set<Plant> plantDiet) {
        this.plantDiet = plantDiet;
    }

    public double getMaxFoodSize() {
        return maxFoodSize;
    }
    public void setMaxFoodSize(double maxFoodSize) {
        this.maxFoodSize = maxFoodSize;
    }

    public void addPlantToDiet(Plant plant){
        if (plant!=null){
            plantDiet.add(plant);
        }
    }

    @Override
    public String toString() {
        return "Omnivore{" + " name='" + name + '\'' + ", weight=" + weight + ", height=" + height + ", length=" + length + ", plantDiet=" + plantDiet + ", maxFoodSize=" + maxFoodSize + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Omnivore omnivore = (Omnivore) o;
        return Double.compare(omnivore.maxFoodSize, maxFoodSize) == 0 && Objects.equals(plantDiet, omnivore.plantDiet);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), plantDiet, maxFoodSize);
    }
}
