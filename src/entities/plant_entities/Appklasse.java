package entities.plant_entities;

import java.util.stream.Stream;

public class Appklasse {
    public static void main(String[] args){
        Plant[] plants = {new Flower("PinkDelight",10, Scent.FARTHY),
                new Flower("PinkDelight",5,Scent.ORANGE),
                new Bush("PinkDelight",25, LeafType.HEART),
                new Tree("PinkDelight",15,LeafType.NEEDLE),
                new Weed("PinkDelight", 5,10),
                new Flower("PinkDelight",5,Scent.ORANGE)};

        Stream.of(plants).forEach(System.out::println);
    }
}
