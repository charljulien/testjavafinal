package entities.plant_entities;

import java.util.Objects;

public class Weed extends Plant{
    private double area;

    public Weed(String name) {
        super(name);
    }
    public Weed(String name, double height,double area) {
        super(name, height);
        this.area=area;
    }

    public double getArea() {
        return area;
    }
    public void setArea(double area) {
        this.area=area;
    }

    @Override
    public String toString() {
        return "Weed{" + " name='" + name + '\'' + ", height=" + height + ", area=" + area + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Weed weed = (Weed) o;
        return Double.compare(weed.area, area) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), area);
    }
}
